package Dist::Zilla::Plugin::FatPacker;
# ABSTRACT: pack your script's deps with App::FatPacker

use strict;
use warnings;

our $VERSION = '0.01';

use Moose;
with 'Dist::Zilla::Role::FileMunger';

use File::Which qw(which);
use File::Path qw(rmtree);
use App::FatPacker;
use namespace::autoclean;

has file => (
    is  => 'ro',
    isa => 'ArrayRef[Str]',
    default => sub { die "[FatPacker] requires 'file' attribute(s)\n" }
);

sub mvp_multivalue_args { return qw(file) }

sub munge_file {
    my ($self, $file) = @_;
    $self->_fatpack($file) if grep { $file->name eq $_ } @{$self->file};
    return;
}

sub _fatpack {
    # Until a version of App::FatPacker that has a stable programmatic interface
    # is released, we are going to have to use the 'fatpack' CLI application.
    my ($self, $file) = @_;
    die "cannot find a 'fatpack' executable in PATH\n" unless which('fatpack');
    my $file_name = $file->name;
    # later, we will want to remove the fatlib if it did not already exist.
    my $already_had_fatlib = -d 'fatlib';
    my $file_fatpacked = qx(fatpack pack $file_name);
    rmtree('fatlib') if !$already_had_fatlib && -d 'fatlib';
    die "fatpack command exited with non-zero status $?\n" unless $? == 0;
    $file->content($file_fatpacked);
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head1 NAME

Dist::Zilla::Plugin::FatPacker - pack your script's deps with App::FatPacker

=head1 SYNOPSIS

In your dist.ini

    [FatPacker]
    file = bin/program.pl

=head1 DESCRIPTION

The usage of [FatPacker] in the example above will cause the file bin/program.pl
to be a fatpacked version of itself in the output distribution.

If you want multiple files to be fatpacked, just add more C<file = SCRIPT> lines
under [FatPacker].

=head1 AUTHOR

Nicholas Hubbard <nicholashubbard@posteo.net>

=head1 COPYRIGHT

This software is Copyright (c) 2023 by Nicholas Hubbard.

This is free software, licensed under the MIT license.

=cut
