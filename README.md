# Dist-Zilla-Plugin-FatPacker

## Description

A [Dist::Zilla](https://metacpan.org/pod/Dist::Zilla) plugin to use [App::FatPacker](https://metacpan.org/pod/App::FatPacker) to fatpack your script.

## Installation

```
cpanm Dist::Zilla::Plugin::FatPacker
```

## Developers Only

```
### install dependencies
$ dzil authordeps --missing | cpanm
$ dzil listdeps | cpanm

### build distribution
$ dzil build

### test distribution
$ dzil test

### release to CPAN
$ dzil release
```
