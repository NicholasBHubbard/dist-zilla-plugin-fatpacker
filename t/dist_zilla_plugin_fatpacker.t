use Test2::V0;
use Test::DZil qw(Builder);
use Path::Tiny;
use Dist::Zilla::Plugin::FatPacker;

my $dist_ini = <<'END_DIST_INI';
name = DZT
abstract = A test distribution
author = Jane Doe <janedoe@example.org>
license = Perl_5
copyright_holder = Jane Doe
copyright_year = 2023
version = 0.01

[GatherDir]
[FatPacker]
file = bin/dzt.pl
END_DIST_INI

my $lib_src = <<'END_LIB_SRC';
package DZT;
sub dzt_foo {
    my ($n1, $n2) = @_;
    $n1 + $n2;
}
1;
END_LIB_SRC

my $bin_src = <<'END_BIN_SRC';
#!/usr/bin/env perl
use lib 'lib';
use DZT;
use File::Which;
print "Hello, World!";
END_BIN_SRC

my $tzil = Builder->from_config(
    { dist_root => 'corpus/DZT' },
    {
        add_files => {
            'source/dist.ini'   => $dist_ini,
            'source/lib/DZT.pm' => $lib_src,
            'source/bin/dzt.pl' => $bin_src
        }
    }
);

$tzil->build;

my $contents = $tzil->slurp_file('build/bin/dzt.pl');

like($contents, qr/sub dzt_foo/, 'lib/DZT.pm is packed');

like($contents, qr/sub dzt_foo/, 'lib/DZT.pm is packed');

# File::Which is a dep of Dist::Zilla::Plugin::FatPacker, so we can be sure it is installed.
like($contents, qr/sub which/, 'File::Which is packed');

my $fatlib = path( $tzil->tempdir )->child('source/fatlib');

ok(! -d $fatlib, 'removed fatlib');

done_testing;
